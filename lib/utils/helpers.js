'use strict'

exports.__esModule = true
exports.notify = notify
exports.instanceId = instanceId
exports.isFirstFocusedRender = isFirstFocusedRender
exports.dateIsInBusinessHours = dateIsInBusinessHours
exports.extractHoursMinutesFromTime = extractHoursMinutesFromTime
var idCount = 0

function uniqueId(prefix) {
  return '' + ((prefix == null ? '' : prefix) + ++idCount)
}

function notify(handler, args) {
  handler && handler.apply(null, [].concat(args))
}

function instanceId(component) {
  var suffix =
    arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : ''

  component.__id || (component.__id = uniqueId('rw_'))
  return (component.props.id || component.__id) + suffix
}

function isFirstFocusedRender(component) {
  return (
    component._firstFocus ||
    (component.state.focused && (component._firstFocus = true))
  )
}

function dateIsInBusinessHours(date, businessHours) {
  var endIncluded =
    arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : false

  // time : HH:MM
  var minutes = date.getMinutes()
  var hours = date.getHours()

  return businessHours.some(function(businessHour) {
    var start = extractHoursMinutesFromTime(businessHour.start)
    var end = extractHoursMinutesFromTime(businessHour.end)

    return (
      ((hours == start.hours && minutes >= start.minutes) ||
        hours > start.hours) &&
      ((hours == end.hours &&
        (endIncluded ? minutes <= end.minutes : minutes < end.minutes)) ||
        hours < end.hours)
    )
  })
}

function extractHoursMinutesFromTime(time) {
  // time : HH:MM
  var values = time.split(':')

  return {
    hours: values[0] ? parseInt(values[0]) : 0,
    minutes: values[1] ? parseInt(values[1]) : 0,
  }
}
