'use strict'

exports.__esModule = true

var _extends =
  Object.assign ||
  function(target) {
    for (var i = 1; i < arguments.length; i++) {
      var source = arguments[i]
      for (var key in source) {
        if (Object.prototype.hasOwnProperty.call(source, key)) {
          target[key] = source[key]
        }
      }
    }
    return target
  }

var _classnames = require('classnames')

var _classnames2 = _interopRequireDefault(_classnames)

var _propTypes = require('prop-types')

var _propTypes2 = _interopRequireDefault(_propTypes)

var _react = require('react')

var _react2 = _interopRequireDefault(_react)

var _propTypes3 = require('./utils/propTypes')

var _BackgroundWrapper = require('./BackgroundWrapper')

var _BackgroundWrapper2 = _interopRequireDefault(_BackgroundWrapper)

var _helpers = require('./utils/helpers')

function _interopRequireDefault(obj) {
  return obj && obj.__esModule ? obj : { default: obj }
}

function _classCallCheck(instance, Constructor) {
  if (!(instance instanceof Constructor)) {
    throw new TypeError('Cannot call a class as a function')
  }
}

function _possibleConstructorReturn(self, call) {
  if (!self) {
    throw new ReferenceError(
      "this hasn't been initialised - super() hasn't been called"
    )
  }
  return call && (typeof call === 'object' || typeof call === 'function')
    ? call
    : self
}

function _inherits(subClass, superClass) {
  if (typeof superClass !== 'function' && superClass !== null) {
    throw new TypeError(
      'Super expression must either be null or a function, not ' +
        typeof superClass
    )
  }
  subClass.prototype = Object.create(superClass && superClass.prototype, {
    constructor: {
      value: subClass,
      enumerable: false,
      writable: true,
      configurable: true,
    },
  })
  if (superClass)
    Object.setPrototypeOf
      ? Object.setPrototypeOf(subClass, superClass)
      : (subClass.__proto__ = superClass)
}

var TimeSlotGroup = (function(_Component) {
  _inherits(TimeSlotGroup, _Component)

  function TimeSlotGroup() {
    var _temp, _this, _ret

    _classCallCheck(this, TimeSlotGroup)

    for (
      var _len = arguments.length, args = Array(_len), _key = 0;
      _key < _len;
      _key++
    ) {
      args[_key] = arguments[_key]
    }

    return (
      (_ret =
        ((_temp =
          ((_this = _possibleConstructorReturn(
            this,
            _Component.call.apply(_Component, [this].concat(args))
          )),
          _this)),
        (_this.renderSlotTime = function(value) {
          var time = value.toLocaleTimeString()
          return _react2.default.createElement(
            'span',
            { className: (0, _classnames2.default)('rbc-slot-time-hover') },
            time
          )
        }),
        _temp)),
      _possibleConstructorReturn(_this, _ret)
    )
  }

  TimeSlotGroup.prototype.render = function render() {
    var _this2 = this

    var _props = this.props,
      renderSlot = _props.renderSlot,
      resource = _props.resource,
      group = _props.group,
      isSlotDate = _props.isSlotDate,
      slotPropGetter = _props.slotPropGetter,
      businessHours = _props.businessHours,
      Wrapper = _props.timeSlotWrapperComponent

    return _react2.default.createElement(
      'div',
      { className: 'rbc-timeslot-group' },
      group.map(function(value, idx) {
        var slotProps = (slotPropGetter && slotPropGetter(value)) || {}
        var isDisabled = businessHours
          ? !(0, _helpers.dateIsInBusinessHours)(value, businessHours, true)
          : false

        return _react2.default.createElement(
          Wrapper,
          { key: idx, value: value, resource: resource },
          _react2.default.createElement(
            'div',
            _extends({}, slotProps, {
              className: (0, _classnames2.default)(
                'rbc-time-slot',
                slotProps.className,
                !isDisabled && 'rbc-available',
                isDisabled && 'rbc-disabled'
              ),
            }),
            isSlotDate && !isDisabled && _this2.renderSlotTime(value),
            renderSlot && !isDisabled && renderSlot(value, idx)
          )
        )
      })
    )
  }

  return TimeSlotGroup
})(_react.Component)

TimeSlotGroup.propTypes = {
  renderSlot: _propTypes2.default.func,
  timeSlotWrapperComponent: _propTypes3.elementType,
  group: _propTypes2.default.array.isRequired,
  slotPropGetter: _propTypes2.default.func,
  resource: _propTypes2.default.any,
  businessHours: _propTypes2.default.array,
  isSlotDate: _propTypes2.default.bool,
}
TimeSlotGroup.defaultProps = {
  timeSlotWrapperComponent: _BackgroundWrapper2.default,
}
exports.default = TimeSlotGroup
module.exports = exports['default']
