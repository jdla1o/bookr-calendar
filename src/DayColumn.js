import PropTypes from 'prop-types'
import React from 'react'
import { findDOMNode } from 'react-dom'
import cn from 'classnames'

import ReactTooltip from 'react-tooltip'
import ReactDOMServer from 'react-dom/server'

import Selection, { getBoundsForNode, isEvent } from './Selection'
import dates from './utils/dates'
import * as TimeSlotUtils from './utils/TimeSlots'
import { isSelected } from './utils/selection'
import localizer from './localizer'

import { notify, dateIsInBusinessHours } from './utils/helpers'
import { accessor, elementType, dateFormat } from './utils/propTypes'
import { accessor as get } from './utils/accessors'
import * as DayEventLayout from './utils/DayEventLayout'
import TimeSlotGroup from './TimeSlotGroup'

const invertColor = (hex, bw) => {
  if (hex) {
    if (hex.indexOf('#') === 0) {
      hex = hex.slice(1)
    }
    // convert 3-digit hex to 6-digits.
    if (hex.length === 3) {
      hex = hex[0] + hex[0] + hex[1] + hex[1] + hex[2] + hex[2]
    }
    if (hex.length !== 6) {
      throw new Error('Invalid HEX color.')
    }
    var r = parseInt(hex.slice(0, 2), 16),
      g = parseInt(hex.slice(2, 4), 16),
      b = parseInt(hex.slice(4, 6), 16)
  }

  if (bw) {
    return r * 0.299 + g * 0.587 + b * 0.114 > 186 ? '#000000' : '#FFFFFF'
  }
  // invert color components
  r = (255 - r).toString(16)
  g = (255 - g).toString(16)
  b = (255 - b).toString(16)
  // pad each with zeros and return
  // eslint-disable-next-line no-undef
  return '#' + padZero(r) + padZero(g) + padZero(b)
}

class DayColumn extends React.Component {
  static propTypes = {
    events: PropTypes.array.isRequired,
    businessHours: PropTypes.array,

    components: PropTypes.object,
    step: PropTypes.number.isRequired,
    date: PropTypes.instanceOf(Date).isRequired,
    min: PropTypes.instanceOf(Date).isRequired,
    max: PropTypes.instanceOf(Date).isRequired,
    getNow: PropTypes.func.isRequired,

    rtl: PropTypes.bool,
    titleAccessor: accessor,
    tooltipAccessor: accessor,
    allDayAccessor: accessor.isRequired,
    startAccessor: accessor.isRequired,
    endAccessor: accessor.isRequired,

    selectRangeFormat: dateFormat,
    eventTimeRangeFormat: dateFormat,
    eventTimeRangeStartFormat: dateFormat,
    eventTimeRangeEndFormat: dateFormat,
    showMultiDayTimes: PropTypes.bool,
    culture: PropTypes.string,
    timeslots: PropTypes.number,
    messages: PropTypes.object,

    selected: PropTypes.object,
    selectable: PropTypes.oneOf([true, false, 'ignoreEvents']),
    eventOffset: PropTypes.number,
    longPressThreshold: PropTypes.number,

    onSelecting: PropTypes.func,
    onSelectSlot: PropTypes.func.isRequired,
    onSelectEvent: PropTypes.func.isRequired,
    onDoubleClickEvent: PropTypes.func.isRequired,

    className: PropTypes.string,
    dragThroughEvents: PropTypes.bool,
    eventPropGetter: PropTypes.func,
    dayPropGetter: PropTypes.func,
    slotPropGetter: PropTypes.func,
    timeSlotWrapperComponent: elementType,
    eventComponent: elementType,
    eventWrapperComponent: elementType.isRequired,
    resource: PropTypes.any,
    colorResource: PropTypes.any,
    isDragging: PropTypes.any,
  }

  static defaultProps = {
    dragThroughEvents: true,
    timeslots: 2,
  }

  state = { selecting: false }

  constructor(...args) {
    super(...args)

    this.slotMetrics = TimeSlotUtils.getSlotMetrics(this.props)
  }

  componentDidMount() {
    this.props.selectable && this._selectable()
  }

  componentWillUnmount() {
    this._teardownSelectable()
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.selectable && !this.props.selectable) this._selectable()
    if (!nextProps.selectable && this.props.selectable)
      this._teardownSelectable()

    this.slotMetrics = this.slotMetrics.update(nextProps)
  }

  render() {
    const {
      max,
      rtl,
      date,
      getNow,
      selectRangeFormat,
      culture,
      slotPropGetter,
      resource,
      timeSlotWrapperComponent,
      dayPropGetter,
      businessHours,
    } = this.props

    let { slotMetrics } = this
    let { selecting, top, height, startDate, endDate } = this.state

    let selectDates = { start: startDate, end: endDate }

    const { className, style } = (dayPropGetter && dayPropGetter(max)) || {}
    const current = getNow()

    return (
      <div
        style={style}
        className={cn(
          className,
          'rbc-day-slot',
          'rbc-time-column',
          selecting && 'rbc-slot-selecting',
          dates.eq(date, current, 'day') && 'rbc-today'
        )}
      >
        {slotMetrics.groups.map((grp, idx) => (
          <TimeSlotGroup
            key={idx}
            group={grp}
            resource={resource}
            businessHours={businessHours}
            slotPropGetter={slotPropGetter}
            isSlotDate={true}
            timeSlotWrapperComponent={timeSlotWrapperComponent}
          />
        ))}
        <div className={cn('rbc-events-container', rtl && 'rtl')}>
          {this.renderEvents()}
        </div>

        {selecting && (
          <div className="rbc-slot-selection" style={{ top, height }}>
            <span>
              {localizer.format(selectDates, selectRangeFormat, culture)}
            </span>
          </div>
        )}
      </div>
    )
  }

  renderEvents = () => {
    let {
      components: {
        event: EventComponent,
        eventLabel: EventLabelComponent,
        eventHoverContainer: EventHoverComponent,
      },
      culture,
      endAccessor,
      eventPropGetter,
      eventTimeRangeEndFormat,
      eventTimeRangeFormat,
      eventTimeRangeStartFormat,
      eventWrapperComponent: EventWrapper,
      events,
      max,
      messages,
      min,
      rtl: isRtl,
      selected,
      startAccessor,
      titleAccessor,
      colorResource,
    } = this.props

    let styledEvents = DayEventLayout.getStyledEvents({
      events,
      startAccessor,
      endAccessor,
      slotMetrics: this.slotMetrics,
    })

    return styledEvents.map(({ event, style }, idx) => {
      let _eventTimeRangeFormat = eventTimeRangeFormat
      let _continuesPrior = false
      let _continuesAfter = false
      let start = get(event, startAccessor)
      let end = get(event, endAccessor)

      if (start < min) {
        start = min
        _continuesPrior = true
        _eventTimeRangeFormat = eventTimeRangeEndFormat
      }

      if (end > max) {
        end = max
        _continuesAfter = true
        _eventTimeRangeFormat = eventTimeRangeStartFormat
      }

      let continuesPrior = this.slotMetrics.startsBefore(start)
      let continuesAfter = this.slotMetrics.startsAfter(end)

      let title = get(event, titleAccessor)
      let label
      if (_continuesPrior && _continuesAfter) {
        label = messages.allDay
      } else {
        label = localizer.format({ start, end }, _eventTimeRangeFormat, culture)
      }

      let _isSelected = isSelected(event, selected)

      if (eventPropGetter)
        var { style: xStyle, className } = eventPropGetter(
          event,
          start,
          end,
          _isSelected
        )

      let { height, top, width, xOffset } = style

      let wrapperProps = {
        event,
        continuesPrior: _continuesPrior,
        continuesAfter: _continuesAfter,
      }

      let colorText = colorResource && invertColor(colorResource, true)

      let colorStaff = colorResource
        ? {
            backgroundColor: colorResource,
            borderColor: colorResource,
            color: colorText,
          }
        : {}

      const fromApp =
        event.hasOwnProperty('booking_from') &&
        event.booking_from !== 'VENDOR_PORTAL'
          ? 'fromApp'
          : 'fromVendor'

      return (
        <EventWrapper {...wrapperProps} key={'evt_' + idx}>
          <div
            style={{
              ...xStyle,
              ...colorStaff,
              top: `${top}%`,
              height: `${height}%`,
              [isRtl ? 'right' : 'left']: `${Math.max(0, xOffset)}%`,
              width: `${width}%`,
            }}
            onClick={e => this._select(event, e)}
            onDoubleClick={e => this._doubleClick(event, e)}
            onDragStart={() => ReactTooltip.hide()}
            className={cn(`rbc-event ${fromApp}`, className, {
              'rbc-selected': _isSelected,
              'rbc-event-continues-earlier': continuesPrior,
              'rbc-event-continues-later': continuesAfter,
              'rbc-event-continues-day-prior': _continuesPrior,
              'rbc-event-continues-day-after': _continuesAfter,
            })}
            data-for={`hoverEvent_${idx}_${height}_${top}`}
            data-tip-disable={this.activeTooltip}
            data-tip
          >
            {EventLabelComponent ? (
              <EventLabelComponent date={label} event={event} />
            ) : (
              <div className="rbc-event-label">{label}</div>
            )}
            <div className="rbc-event-content">
              {EventComponent ? (
                <EventComponent event={event} title={title} date={label} />
              ) : (
                title
              )}
            </div>
            {EventHoverComponent && (
              <ReactTooltip
                id={`hoverEvent_${idx}_${height}_${top}`}
                className="rbc-hover-container"
                html={true}
                getContent={() =>
                  ReactDOMServer.renderToString(
                    <EventHoverComponent
                      event={event}
                      title={title}
                      date={label}
                    />
                  )
                }
              />
            )}
          </div>
        </EventWrapper>
      )
    })
  }

  _selectable = () => {
    let node = findDOMNode(this)
    let selector = (this._selector = new Selection(() => findDOMNode(this), {
      longPressThreshold: this.props.longPressThreshold,
    }))

    let maybeSelect = box => {
      let { onSelecting, businessHours } = this.props
      let current = this.state || {}
      let state = selectionState(box)
      let { startDate: start, endDate: end } = state

      if (
        businessHours &&
        (!dateIsInBusinessHours(state.startDate, businessHours, true) ||
          !dateIsInBusinessHours(state.endDate, businessHours, true))
      ) {
        return
      }

      if (onSelecting) {
        if (
          (dates.eq(current.startDate, start, 'minutes') &&
            dates.eq(current.endDate, end, 'minutes')) ||
          onSelecting({ start, end }) === false
        )
          return
      }

      if (
        this.state.start !== state.start ||
        this.state.end !== state.end ||
        this.state.selecting !== state.selecting
      ) {
        this.setState(state)
      }
    }

    let selectionState = ({ y }) => {
      let { top, bottom } = getBoundsForNode(node)

      let range = Math.abs(top - bottom)
      let currentSlot = this.slotMetrics.closestSlotToPosition(
        (y - top) / range
      )

      if (!this.state.selecting) this._initialSlot = currentSlot

      let initialSlot = this._initialSlot
      if (initialSlot === currentSlot)
        currentSlot = this.slotMetrics.nextSlot(initialSlot)

      const selectRange = this.slotMetrics.getRange(
        dates.min(initialSlot, currentSlot),
        dates.max(initialSlot, currentSlot)
      )

      return {
        ...selectRange,
        selecting: true,

        top: `${selectRange.top}%`,
        height: `${selectRange.height}%`,
      }
    }

    let selectorClicksHandler = (box, actionType) => {
      if (!isEvent(findDOMNode(this), box)) {
        const { startDate, endDate } = selectionState(box)
        const isDisabled = this.props.businessHours
          ? dateIsInBusinessHours(startDate, this.props.businessHours, true)
          : true
        if (isDisabled) {
          this._selectSlot({
            startDate,
            endDate,
            action: actionType,
            box,
          })
        }
      }
      this.setState({ selecting: false })
    }

    selector.on('selecting', maybeSelect)
    selector.on('selectStart', maybeSelect)

    selector.on('beforeSelect', box => {
      if (this.props.selectable !== 'ignoreEvents') return

      return !isEvent(findDOMNode(this), box)
    })

    selector.on('click', box => selectorClicksHandler(box, 'click'))

    selector.on('doubleClick', box => selectorClicksHandler(box, 'doubleClick'))

    selector.on('select', bounds => {
      if (this.state.selecting) {
        this._selectSlot({ ...this.state, action: 'select', bounds })
        this.setState({ selecting: false })
      }
    })
  }

  _teardownSelectable = () => {
    if (!this._selector) return
    this._selector.teardown()
    this._selector = null
  }

  _selectSlot = ({ startDate, endDate, action, bounds, box }) => {
    let current = startDate,
      slots = []

    while (dates.lte(current, endDate)) {
      slots.push(current)
      current = dates.add(current, this.props.step, 'minutes')
    }

    notify(this.props.onSelectSlot, {
      slots,
      start: startDate,
      end: endDate,
      resourceId: this.props.resource,
      action,
      bounds,
      box,
    })
  }

  _select = (...args) => {
    notify(this.props.onSelectEvent, args)
  }

  _doubleClick = (...args) => {
    notify(this.props.onDoubleClickEvent, args)
  }
}

export default DayColumn
