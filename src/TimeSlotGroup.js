import cn from 'classnames'
import PropTypes from 'prop-types'
import React, { Component } from 'react'

import { elementType } from './utils/propTypes'
import BackgroundWrapper from './BackgroundWrapper'
import { dateIsInBusinessHours } from './utils/helpers'

export default class TimeSlotGroup extends Component {
  static propTypes = {
    renderSlot: PropTypes.func,
    timeSlotWrapperComponent: elementType,
    group: PropTypes.array.isRequired,
    slotPropGetter: PropTypes.func,
    resource: PropTypes.any,
    businessHours: PropTypes.array,
    isSlotDate: PropTypes.bool,
  }
  static defaultProps = {
    timeSlotWrapperComponent: BackgroundWrapper,
  }

  renderSlotTime = value => {
    const time = value.toLocaleTimeString()
    return <span className={cn('rbc-slot-time-hover')}>{time}</span>
  }

  render() {
    const {
      renderSlot,
      resource,
      group,
      isSlotDate,
      slotPropGetter,
      businessHours,
      timeSlotWrapperComponent: Wrapper,
    } = this.props

    return (
      <div className="rbc-timeslot-group">
        {group.map((value, idx) => {
          const slotProps = (slotPropGetter && slotPropGetter(value)) || {}
          const isDisabled = businessHours
            ? !dateIsInBusinessHours(value, businessHours, true)
            : false

          return (
            <Wrapper key={idx} value={value} resource={resource}>
              <div
                {...slotProps}
                className={cn(
                  'rbc-time-slot',
                  slotProps.className,
                  !isDisabled && 'rbc-available',
                  isDisabled && 'rbc-disabled'
                )}
              >
                {isSlotDate && !isDisabled && this.renderSlotTime(value)}
                {renderSlot && !isDisabled && renderSlot(value, idx)}
              </div>
            </Wrapper>
          )
        })}
      </div>
    )
  }
}
